package com.example.demo;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringlabdeployApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringlabdeployApplication.class, args);
//		ApplicationContext ctx = SpringApplication.run(SpringlabdeployApplication.class, args);
//		Arrays.asList(ctx.getBeanDefinitionNames()).forEach(System.out::println);
	}

}
